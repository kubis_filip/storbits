from decimal import Decimal
from math import pi, sqrt, pow, sin, asin, tan, atan, radians, degrees
from astropy.constants import G, R_earth, M_earth

# TODO improve global variables handling
__turn = 15  # turn duration in minutes
__earth_ang_vel = 0.25  # angular velocity of earth in degree/minute


def get_period(height):
    return sqrt((4*pow(pi, 2))/(G.value * M_earth.value)*pow(R_earth.value + float(height*1000), 3)) / 60


def __format_val(val, modulo):
    return val % modulo


def __get_ang_change(ang_vel, time):
    return Decimal(ang_vel * time)


# Orbital part
def get_new_mean(ship, reset):
    if reset == "true":
        __new_mean = Decimal(0)
    else:
        __ang_vel = 360 / get_period(ship.height)
        __mean = ship.mean
        __new_mean = __format_val(__mean + __get_ang_change(__ang_vel, __turn), 360)
    return round(__new_mean, 6)


# Location part
# Get new longitude of ascending node
def get_new_lan(ship):
    return ship.lan - __get_ang_change(__earth_ang_vel, get_period(ship.height))


def get_lat(m, i):
    mean = radians(m)
    inc = radians(i)
    return round(degrees(asin(sin(mean)*sin(inc))), 6)


def get_lon(ln, m, i, lt):
    lan = radians(ln)
    mean = radians(m)
    inc = radians(i)
    lat = radians(lt)
    return round(degrees(lan + 2*atan(tan((mean - lat)/2) * (sin((pi/2 + inc)/2) / sin((pi/2 - inc)/2)))), 6)


def get_path(ship):
    coord_points = []
    for i in range(-180, 185, 5):
        lt = get_lat(i, ship.inclination)
        ln = get_lon(ship.lan, i, ship.inclination, lt)
        point = {
            'x': ln,
            'y': lt
        }
        coord_points.append(point)
        print(i, ln)
    return coord_points
