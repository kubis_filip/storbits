from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('list/', views.ship_list, name="ship_list"),
    path('<str:ship>/', views.stats, name='stats'),

    path('utils/make_turn/', views.make_turn, name="make_turn"),
    path('utils/update_list/', views.update_list, name="update_list"),
    path('utils/get_path/', views.get_path, name="get_path"),
]
