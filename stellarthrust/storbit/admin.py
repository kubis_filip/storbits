from django.contrib import admin
from .models import Ship, Icon

admin.site.register(Ship)
admin.site.register(Icon)
