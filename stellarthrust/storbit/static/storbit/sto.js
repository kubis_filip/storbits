$(document).ready(function () {
    //------------------------------------
    //Django basic setup for accepting ajax requests.
    // Cookie obtainer Django

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');
    // Setup ajax connections safetly

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    //---------------------------------------

    $('.mkturn').bind('click', function (){
        var ship = $(this).attr('ship');
        var reset = $(this).hasClass('reset');
        makeTurn(ship, reset);
    })

    $('.mkturn-all').bind('click', function (){
        const ships = $(this).attr('ships')
        var reset = $(this).hasClass('reset');
        update_list(ships, reset);
    })
})

function update_list(ships, reset) {
    $.ajax({
    url: '/storbit/utils/update_list/',
    data: {
        'ships': ships,
        'reset': reset,
    },
    dataType: 'json',
    type: 'POST',
    success: function(data) {
        var values = JSON.parse(data);
        for(var i = 0; i < values.length; i++) {
            $('.'+values[i].name+'-mean').text(values[i].mean + "°");
            }
        }
    });
}

function makeTurn(ship, reset) {
    $.ajax({
    url: '/storbit/utils/make_turn/',
    data: {
        'ship': ship,
        'reset': reset,
    },
    dataType: 'json',
    type: 'POST',
    success: function(data) {
        $('#mean').text(data.new_mean + "°");
        $('.lat').text(data.lat + "°");
        $('.lon').text(data.lon + "°");
        draw_path(data.path_points, data.lat, data.lon)
        }
    });
}

function draw_path(path_points, lat, lon) {
    points = {
        series:[{
            name: 'path',
            data: path_points
        }, {
            name: 'loc',
            data: [{
                x: lon,
                y: lat,
            }]
        }
        ]
    };

    options = {
        axisX: {
            type: Chartist.AutoScaleAxis,
            high: 170,
            low: -170,
            showGrid: false,
            showLabel: false
        },
        axisY: {
            type: Chartist.AutoScaleAxis,
            high: 90,
            low: -90,
            showGrid: false,
            showLabel: false
        },
        series: {
            'path': {
                showPoint: false,
            },
        }
    };

    new Chartist.Line('.ct-chart', points, options)
}