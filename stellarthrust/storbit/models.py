from django.db import models


class Icon(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name + ".png"


class Ship(models.Model):
    name = models.CharField(max_length=30)
    height = models.FloatField(default=300.0)
    inclination = models.DecimalField(max_digits=9, decimal_places=6, default=45)
    lan = models.DecimalField(max_digits=9, decimal_places=6, default=0)
    mean = models.DecimalField(max_digits=9, decimal_places=6, default=0)
    icon = models.ForeignKey(Icon, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
