from django.shortcuts import render
from django.http import JsonResponse
from .models import Ship
from .modules import STCalc
import json


def index(request):
    return render(request, 'storbit/index.html')


def ship_list(request):
    ships = Ship.objects.order_by('height')
    context = {'ships': ships}
    return render(request, 'storbit/list.html', context)


def stats(request, ship):
    ship = Ship.objects.get(name=ship)
    lat = STCalc.get_lat(ship.mean, ship.inclination)
    lon = STCalc.get_lon(ship.lan, ship.mean, ship.inclination, lat)
    path = STCalc.get_path(ship)
    # print(path)
    context = {'ship': ship, 'lat': lat, 'lon': lon, 'path': path}
    return render(request, 'storbit/stats.html', context)


def make_turn(request):
    ship = Ship.objects.get(name=request.POST.get('ship', None))
    reset = request.POST.get('reset', None)

    if request.is_ajax():
        mean = update_mean(ship, reset)

        lat = STCalc.get_lat(mean, ship.inclination)
        lon = STCalc.get_lon(ship.lan, mean, ship.inclination, lat)

        path_points = STCalc.get_path(ship)

        data = {
            'ship': ship.name,
            'new_mean': mean,
            'lat': lat,
            'lon': lon,
            'path_points': path_points,
        }
        return JsonResponse(data)


def update_list(request):
    ships = request.POST.get('ships', None).split()
    reset = request.POST.get('reset', None)

    if request.is_ajax():
        data = []
        for name in ships:
            mean = update_mean(name, reset)
            val = {'name': name, 'mean': float(mean)}
            data.append(val)
        return JsonResponse(json.dumps(data), safe=False)


def update_mean(ship, reset):
    ship = Ship.objects.get(name=ship)
    ship.mean = STCalc.get_new_mean(ship, reset)
    # TODO update lan
    ship.save()
    return ship.mean


def get_path(request):
    if request.is_ajax():
        return JsonResponse({'path': STCalc.get_path(Ship.objects.get(name=request.GET.get('ship', None)))})
